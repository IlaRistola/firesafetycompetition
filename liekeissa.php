<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="scripts/noiseShader.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <title>Fire-Game</title>
    

<script src="scripts/gameregcomp.js"></script>

<style>
  html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
}
.es {
opacity:1;
position: fixed;
display: none;
z-index: 2;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
width: 512px;
height: 512px;
max-width: 100%;
max-height: 100%;
background-image: url('content/visuals/Game/f4.png');
background-size: 100%;
background-repeat: no-repeat;
text-align: center; 
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}
.es.eng{
background-image: url('content/visuals/Game/f4eng.png');
}
.es.swe{
background-image: url('content/visuals/Game/f4swe.png');
}

.st {
opacity:1;
position: fixed;
display: none;
z-index: 2;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
width: 512px;
height: 512px;
max-width: 100%;
max-height: 100%;
background-image: url('content/visuals/Game/f1.png');
background-size: 100%;
background-repeat: no-repeat;
text-align: center; 
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}
.st.eng{
background-image: url('content/visuals/Game/f1en.png');
}
.st.swe{
background-image: url('content/visuals/Game/fiswe.png');
}

.sc {
opacity:1;
position: fixed;
display: none;
z-index: 2;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
width: 512px;
height: 512px;
max-width: 100%;
max-height: 100%;
background-image: url('content/visuals/Game/f3.png');
background-size: 100%;
background-repeat: no-repeat;
text-align: center; 
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}
.sc.eng{
background-image: url('content/visuals/Game/f3en.png');
}
.sc.swe{
background-image: url('content/visuals/Game/f3swe.png');
}

#yourPoints{
opacity:1;
z-index: 2;
padding: 30% 0;
display: inline-block;
vertical-align: middle;
/*line-height: 500px; /* <-- adjust this */
font-style: normal;
font-weight: bold;
color: white;
font-size: 40px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#gameTime {
  display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#myProgress {
display: none;
position: fixed;
width: 5%;
height: 25%;
left: 5%;
top: 70%;
background-color: grey;
z-index: 3;
transform: rotateZ(180deg);
}

#myBar {
width: 100%;
height: 100%;
z-index: 3;
background-color: #4CAF50;
}
</style>

</head>

<body>
<div id="endScreen" class="es" onclick="newBurning()"><p id="yourPoints">FULL</p></div>
<div id="startTutorial" class="st" onclick="startTutorial()"></div>
<div id="startTest" class="sc" onclick="startChallenge()"></div>
<p id="gameTime">HARJOITUSKIERROS</p>
<div id="myProgress">
  <div id="myBar"></div>
</div>
   <!-- Fire game happens in 360 view with 3d and 2d contents like fire and foam -->
 <a-scene vr-mode-ui="enabled: false" loading-screen="enabled: false">

        <a-assets>
                <img id="stillView" preload="auto" src="https://d3d7hmdbenc9d5.cloudfront.net/game/gameRoom.png" crossOrigin="anonymous" playsinline webkit-playsinline>
                <img id="stillView2" preload="auto" src="https://d3d7hmdbenc9d5.cloudfront.net/game/gameRoom2.png" crossOrigin="anonymous" playsinline webkit-playsinline>
                <img id="foam" preload="auto" src="content/visuals/Game/f8.png"/>
                <img id="sheet" src="content/visuals/Game/fire2.png"/>
                <video id="tutorialVideo" src="content/visuals/Game/F1_1nh.mp4" preload="auto" crossorigin="anonymous" playsinline webkit-playsinline></video>
              </a-assets>
        
         <!--ROOM--> 
         <a-sky id="imgSphere2" src="#stillView2" rotation="0 0 0" visible="true" opacity="1">
        </a-sky>     
            <a-sky id="imgSphere" src="#stillView" rotation="0 0 0" visible="true" opacity="1"   
        animation="property: opacity; from: 1; to: 0; dur: 60000; startEvents: opa; pauseEvents: apo">
            </a-sky>

        <!--FIRES-->
        <a-image id="fire" position="0 -1 -9" rotation="0 0 0" scale="10 10" opacity="0.7" visible="false" material="shader: flat; src: #sheet; transparent: true;" spritesheet-animation="rows: 4; columns: 8; frameDuration: 0.08; loop: true;"></a-image>
        <a-entity id="tutorialTargets" visible="false">
        <a-circle position="0 -0.9 -4" class="link" rotation="0 0 0" radius="0.2" color="green" opacity="0.6" id="T1" ft1></a-circle>
        <a-ring position="0 -0.9 -4" class="link" rotation="0 0 0" geometry="radiusInner: 0.2; radiusOuter: 0.4" color="green" opacity="0.4" id="T2" ft11></a-ring>
        <a-circle position="0 -0.9 -4.5" class="link" rotation="0 0 0" scale="0.6 0.9" color="green" opacity="0.2" id="T3" ft111></a-circle>
        </a-entity>
        <!--FIRE 1--> 
        <a-image id="fire1" position="-11 0 0.5" rotation="0 -45 0" scale="5 5" opacity="0.7" visible="false" material="shader: flat; src: #sheet; transparent: true;" spritesheet-animation="rows: 4; columns: 8; frameDuration: 0.08; loop: true;"></a-image>
        <a-circle position="-11 -1.5 0.5" class="link" rotation="0 120 0" radius="0.3" visible="false" color="red" opacity="0.7" id="target1" f1></a-circle>
        <a-ring position="-11 -1.5 0.5" class="link" rotation="0 120 0" geometry="radiusInner: 0.3; radiusOuter: 0.6" visible="false" color="yellow" opacity="0.7" id="target11" f11></a-ring>
        <a-circle position="-12.5 -1.2 0.8" class="link" rotation="0 120 0" scale="0.8 0.9" color="green" visible="false" opacity="0.7" id="target1" f111></a-circle>
        <a-plane id="foam1" position="-10 -1.2 0.5" src="#foam" rotation="0 120 0" scale="1.5 1.5" visible="false" opacity="0.7" material="shader: flat; transparent: true;"></a-plane>

        <!-- wake up second fire at scale 7 7 -->
        <a-image id="fire1_2" position="-9 -1 2" rotation="0 -45 0" scale="2 1" opacity="0" visible="false" material="shader: flat; src: #sheet; transparent: true;" spritesheet-animation="rows: 4; columns: 8; frameDuration: 0.08; loop: true;"
        animation="property: opacity; to: 0.9; dur: 5000; easing: linear; startEvents: opa; loop: false"></a-image>
        <a-circle position="-8 -0.9 2" class="link" rotation="0 120 0" radius="0.15" visible="false" color="red" opacity="0.7" id="target1" f1_2></a-circle>
        <a-ring position="-8 -0.9 2" class="link" rotation="0 120 0" geometry="radiusInner: 0.15; radiusOuter: 0.3750" visible="false" color="blue" opacity="0.7" id="target11" f1_22></a-ring>
        <a-circle position="-10 -1 2.5" class="link" rotation="0 120 0" scale="0.6 0.8" color="green" visible="false" opacity="0.7" id="target1" f1_222></a-circle>
        <a-plane id="foam1_2" position="-7 -0.8 1.8" src="#foam" rotation="0 120 0" scale="1 1" visible="false" opacity="0.7" material="shader: flat; transparent: true;"></a-plane>

        <!-- FIRE 2 -->
        <a-image id="fire2_2" position="4 0.16 8" rotation="0 -120 0" scale="2 2" visible="false" opacity="0" material="shader: flat; src: #sheet; transparent: true;" spritesheet-animation="rows: 4; columns: 8; frameDuration: 0.08; loop: true;"
        animation="property: opacity; to: 0.9; dur: 5000; easing: linear; startEvents: opa; loop: false"></a-image>
        <a-circle position="4 0 8" class="link" rotation="0 -130 0" radius="0.2" visible="false" color="red" opacity="0.7" id="target1" f2_2></a-circle>
        <a-ring position="4 0 8" class="link" rotation="0 -130 0" geometry="radiusInner: 0.2; radiusOuter: 0.5" visible="false" color="blue" opacity="0.7" id="target11" f2_22></a-ring>
        <a-circle position="4.5 0.5 9" class="link" rotation="0 -130 0" scale="0.7 0.8" color="green" visible="false" opacity="0.7" id="target1" f2_222></a-circle>
        <a-plane id="foam2_2" position="3.5 0 7.5" src="#foam" rotation="0 -130 0" scale="1.5 1.5" visible="false" opacity="0.7" material="shader: flat; transparent: true;"></a-plane>
        
        <a-image id="fire2" position="3 0.2 2.7" rotation="0 -130 0" scale="1 2" visible="false" opacity="0.7" material="shader: flat; src: #sheet; transparent: true;" spritesheet-animation="rows: 4; columns: 8; frameDuration: 0.08; loop: true;"></a-image>
        <a-circle position="3 -0.25 2.7" class="link" rotation="0 -130 0" radius="0.075" visible="false" color="red" opacity="0.7" id="target1" f2></a-circle>
        <a-ring position="3 -0.25 2.7" class="link" rotation="0 -130 0" geometry="radiusInner: 0.075; radiusOuter: 0.1875" visible="false" color="blue" opacity="0.7" id="target11" f22></a-ring>
        <a-circle position="4.1 -0.60 3.7" class="link" rotation="0 -130 0" scale="0.4 0.5" color="green" visible="false" opacity="0.7" id="target1" f222></a-circle>
        <a-plane id="foam2" position="2.5 0 2.5" src="#foam" rotation="0 -130 0" scale="1 1" visible="false" opacity="0.7" material="shader: flat; transparent: true;"></a-plane>
     
        <!-- CAMERA-->
            <a-entity look id="cameraWrapper" rotation="0 0 0" resetorientation>
                <a-camera look-controls mouse-cursor wasd-controls-enabled="false">
                <a-cursor
                  id="cursor"
                  animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 150"
                  animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500"
                  event-set__mouseenter="_event: mouseenter; color: springgreen"
                  event-set__mouseleave="_event: mouseleave; color: black"
                  visible="false"
                  raycaster="objects: .link" foo>
                </a-cursor>
                <a-entity cursor="rayOrigin: mouse" raycaster="objects: .touchable"></a-entity>
                <a-sphere id="foamSphere" visible="false" geometry="radius: 0.2" scale="0.4 4.3" rotation="0 180 -11" material="shader: displacement-offset" position="0.12 -0.65 -3" myoffset-updater>
              </a-sphere>  
                <a-plane src="content/visuals/Game/fe2.png" id="fireEx" class="not-touchable" position="0 -0.85 -1" scale="0.5 1 0.5" material="shader: flat; transparent: true;" visible="false"></a-plane>
                <a-video src="#tutorialVideo" id="tutorialLayer" position="0 0 -2" scale="1.5 1.5" visible="false"></a-video>
              </a-camera>
            </a-entity>

    </a-scene>

    <script src="scripts/fireGame.js"></script>

</body>
</html>