<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sammutus</title>
    <style>
      html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
}
.toplogo {
width: 25%;
height: auto;
display: block;
margin-left: auto;
margin-right: auto;
}

.visa-end {
  width: 50%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  background-color: rgba(187, 187, 187, 0.5);
  padding: 20px 25px;
}

.visaEndText{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}

.itemA { grid-area: menua;}
.itemA2 { grid-area: maina; }
.itemB { grid-area: menub; }
.itemB2 { grid-area: mainb; }
.itemC { grid-area: menuc; }
.itemC2 { grid-area: mainc; }
.itemD { grid-area: menud; }
.itemD2 { grid-area: maind; }
.itemE { grid-area: menue;}
.itemE2 { grid-area: maine; }
.itemF { grid-area: menuf; }
.itemF2 { grid-area: mainf; }
.itemG { grid-area: menug; }
.itemG2 { grid-area: maing; }
.itemH { grid-area: menuh; }
.itemH2 { grid-area: mainh; }
.item5 { 
    grid-area: num;
    }
.item6 { grid-area: quest;
 }
.item7 { grid-area: qtext;
 }

.grid-outside {
  width: 50%;
  display: none;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: rgba(187, 187, 187, 0.5);
  position: relative;
  z-index: 2;
}
.startTestDiv {
  width: 50%;
  top: 50%;
  left: 50%;
  display: none;
  transform: translate(-50%, -50%);
  background-color: #1d1d1b;
  padding:5px 5px;
  position: relative;
  z-index: 2;
}

.grid-container {
  display: grid;
  grid-template-areas:
    'menua maina maina maina maina maina'
    'menub mainb mainb mainb mainb mainb'
    'menuc mainc mainc mainc mainc mainc'
    'menud maind maind maind maind maind';
  grid-gap: 5px;
  padding: 1px;
}
.grid-container2 {
  display: grid;
  grid-template-areas:
    'menua maina maina maina maina maina'
    'menub mainb mainb mainb mainb mainb'
    'menuc mainc mainc mainc mainc mainc'
    'menud maind maind maind maind maind'
    'menue maine maine maine maine maine'
    'menuf mainf mainf mainf mainf mainf'
    'menug maing maing maing maing maing'
    'menuh mainh mainh mainh mainh mainh';
  grid-gap: 5px;
  padding: 1px;
}
.question {
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: 10vw;
  grid-template-areas:
    'num quest quest quest quest'
    'num qtext qtext qtext qtext';
  grid-gap: 0em;
  grid-column-gap: 0em;
  grid-auto-flow: dense;
  height: auto;
  margin-bottom: 10px;
}

.question .item5, .question .item7{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}
.question .item6{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-style: italic;
    background-color: #1d1d1b;
    color: #d4d3d3;
    font-size: 1vw;
}

.question > div {
  text-align: left;
  padding: 3px 20px; 
  font-size: 18px;
  height: auto;
}

.qnum {
    position: absolute;
    background-color: #e0101d;
    color: white;
  text-align: center;
  vertical-align: center;
  padding: 5px 10;
  margin-top: -10px;
  width: auto;
  font-size: 3.5vw;
  font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: bold;
  font-style: italic;
  transform-origin: top left;
  transform: skewX(-12deg);
}

.startTestDiv .startBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: center;
    padding: 10px 5;
    margin-left: 40%;
    margin-bottom: 5%;
}
.misNote{
  background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 1.2vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5%;
}
.grid-container .itemA, .grid-container .itemB, .grid-container .itemC, .grid-container .itemD{
    background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 3vw;
  padding: 3px 0;
}
.grid-container .itemA2, .grid-container .itemB2, .grid-container .itemC2, .grid-container .itemD2{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: left;
    padding: 10px 5;
}

.grid-container2 .itemA, .grid-container2 .itemB, .grid-container2 .itemC, .grid-container2 .itemD, .grid-container2 .itemE, .grid-container2 .itemF, .grid-container2 .itemG, .grid-container2 .itemH{
    background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 3vw;
  padding: 3px 0;
}
.grid-container2 .itemA2, .grid-container2 .itemB2, .grid-container2 .itemC2, .grid-container2 .itemD2, .grid-container2 .itemE2, .grid-container2 .itemF2, .grid-container2 .itemG2, .grid-container2 .itemH2{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: left;
    padding: 10px 5;
    outline: none;
}

.btn-group{
    z-index: 2;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: block;
}
.btn-group button {
margin: 1.5vw;
padding: 10vh;
cursor: pointer;
float: left;
border-style: hidden;
}


#TBD {
opacity:1;
position: fixed;
z-index: 1;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
}
#quizPoints {
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

#gameTime {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#videoKierros {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 82%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

.queImage{
  width: 850px;
  height: 650px;
  display: none;
  top: 45%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: rgba(187, 187, 187, 0.5);
  position: relative;
  z-index: 2;
}

.qimg {
height: 480px;
width: 720px;
margin-left: auto;
margin-right: auto;
margin-top: 5%;
margin-bottom: 2%;
padding-top: 5%;
padding-bottom: 1%;
}

.picBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 2.5vw;
    padding: 10px;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 2%;
    margin-top: 2%;
}

    </style>

</head>

<body>

  <p id="gameTime"></p>
  <p id="videoKierros"></p>
  <div class="startTestDiv" id="startDivi">
    <div class="missionGuide"><div class="misNote" id="thisNote">Tehtävänne on katsoa videot ja vastata kolmeen visaiseen kysymykseen. Lukekaa kysymykset tarkkaan ja valitkaa oikea vastaus tai oikea järjestys toimia kysymyksestä riippuen. Vastausvaihtoehdot ilmestyvät ruudulle useamman kerran. Valitkaa vastauksenne tarkkaan, sillä tehtyä valintaa EI voi muuttaa.</div></div>
    <button class="startBut" id="stbut" onclick="startTest()">Aloita testi</button>
  </div>
  <div class="queImage" id="questImg" style="vertical-align:middle; text-align:center">
  <!-- vids was added locally to experience because late changes. Better way is to use them from amazon bucket -->
    <video class="qimg" id="vid1" src="content/vids/sammutus/v1.mp4" style="display: block;" preload="auto" crossOrigin="anonymous" playsinline webkit-playsinline></video>
    <video class="qimg" id="vid2" src="content/vids/sammutus/v2.mp4" style="display: none;" preload="auto" loop="false" crossOrigin="anonymous" playsinline webkit-playsinline></video>
    <video class="qimg" id="vid3" src="content/vids/sammutus/v3.mp4" style="display: none;" preload="auto" loop="false" crossOrigin="anonymous" playsinline webkit-playsinline></video>
     <button class="picBut" id="picbut" onclick="nextOne()">JATKA</button>
  </div>
  <div class="grid-outside" id="gOutside">
    <div class="qnum" id="qRound">1/1</div>
      <div class="question"><div class="item5"></div><div class="item6">Tehtävänanto</div><div class="item7" id="description">Kotkot</div></div>
      <!-- missions questions coming from json list -->
    <div class="grid-container2">
      <div class="itemA">A</div>
      <button class="itemA2" id="ia2" onclick="optA()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemB">B</div>
      <button class="itemB2" id="ib2" onclick="optB()">Pysähdyn paikalle kysymään, voinko auttaa jotenkin.</button> 
      <div class="itemC" id="ic" style="display: none">C</div>
      <button class="itemC2" id="ic2" onclick="optC()" style="display: none">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button> 
      <div class="itemD" id="id" style="display: none">D</div>
      <button class="itemD2" id="id2" onclick="optD()" style="display: none">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemE" id="ie" style="display: none">E</div>
      <button class="itemE2" style="display: none" id="ie2" onclick="optE()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemF" id="if" style="display: none">F</div>
      <button class="itemF2" style="display: none" id="if2" onclick="optF()">Pysähdyn paikalle kysymään, voinko auttaa jotenkin.</button> 
      <div class="itemG" id="ig" style="display: none">G</div>
      <button class="itemG2" style="display: none" id="ig2" onclick="optG()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button> 
      <div class="itemH" id="ih" style="display: none">H</div>
      <button class="itemH2" style="display: none" id="ih2" onclick="optH()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>    
    </div>
  </div>

<script>

  
      // created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //

  /* This mission was made fast just before launch, so there can be some weird shortcuts */
  var json = (function() {
    // Real question list is hidden from public in gitlab
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': "jsons/samthree.json",
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();

if (typeof(Storage) !== "undefined") {
        // Store
        game = sessionStorage.getItem("sammutus");
        if(game){
          window.location.href="menu.php";
        } else{
            sessionStorage.setItem("sammutus", "true");
          }
        } else{
        alert("cant access storage");
        }

qa = document.getElementById("ia2");
qb = document.getElementById("ib2");
qc = document.getElementById("ic2");
qic = document.getElementById("ic");
qd = document.getElementById("id2");
qid = document.getElementById("id");
qe = document.getElementById("ie2");
qie = document.getElementById("ie");
qf = document.getElementById("if2");
qif = document.getElementById("if");
qg = document.getElementById("ig2");
qig = document.getElementById("ig");
qh = document.getElementById("ih2");
qih = document.getElementById("ih");
desc = document.getElementById("description");
qrnd = document.getElementById("qRound");
sDiv = document.getElementById("startDivi");
qGrid = document.getElementById("gOutside");
qImgGrid = document.getElementById("questImg");
qpic = document.getElementById("qimg");
var wl = json;
var qlist = [wl.q1,wl.q2,wl.q3,wl.q4,wl.q5,wl.q6,wl.q7,wl.q8,wl.q9,wl.q10,wl.q11,wl.q12,wl.q13,wl.q14,wl.q15];
var q = 0;
var wlq = qlist[q];
var que = wlq.a;
var que2 = wlq.b;
var que3 = wlq.c;
var que4 = wlq.d;
var que5 = wlq.e; //
var que6 = wlq.f;
var que7 = wlq.g;
var que8 = wlq.h;
var dee = wlq.des;
var one = que.answer;
var two = que2.answer;
var five;
var six;
var seven;
var eigth; 
var deed = dee.question;
var qRounder = 0;
var points = 0;
var dblQ = 1;
var dbchk = false;
var level = 0;
var qRoundLimit = 1;
var timer = 900;
totalPoints = 0;
var playTime = document.getElementById("gameTime");
var videoRd = document.getElementById("videoKierros");
var end = false;
vid1 = document.getElementById("vid1");
vid2 = document.getElementById("vid2");
vid3 = document.getElementById("vid3");
video = vid1;

  sDiv.style.display = "block";

function Clock(duration, display){
var timing = duration, minutes, seconds;
tutoTime = setInterval(function(){ 
timer--;
minutes = parseInt(timer / 60, 10);
seconds = parseInt(timer % 60, 10);
minutes = minutes < 10 ? "0" + minutes : minutes;
seconds = seconds < 10 ? "0" + seconds : seconds;
playTime.innerHTML = minutes + ":" + seconds;
if(timer == 0){
  backToMenu();
}
}, 1000);
}

function startTest() {
  sDiv.style.display = "none";
  qImgGrid.style.display = "block";
  playTime.style.display = "block";
  video.play();
  Clock();
}

function nextOne() {
    console.log("next");
  video.pause();
  qRounder = 0;
  qImgGrid.style.display = "none";
  qGrid.style.display = "block";
  level++;
  newQ();
}

function nextVid() {
    video.play();
    qGrid.style.display = "none";
  qImgGrid.style.display = "block";
}

video.addEventListener('ended', function(e) {
nextOne();
});

function videoLoader() {
if(level == 1){
vid1.style.display = "none";
vid2.style.display = "block";
video = vid2;
}  else if(level == 5){
  vid2.style.display = "none";
vid3.style.display = "block";
  video = vid3;
} else if(level == 7){
  end = true;
}
video.currentTime = 0;
}

function newQ() {
if(level > 0){
videoLoader();
}
wlq = qlist[q];
que = wlq.a;
que2 = wlq.b;
if(wlq.c){
que3 = wlq.c;
three = que3.answer;
qc.innerHTML = three;
qc.style.display = "block";
qic.style.display = "block";
} else{
  qc.style.display = "none";
  qic.style.display = "none";
}
if(wlq.d){
que4 = wlq.d;
four = que4.answer;
qd.innerHTML = four;
qd.style.display = "block";
qid.style.display = "block";
} else{
  qd.style.display = "none";
  qid.style.display = "none";
}
if(wlq.e){
que5 = wlq.e;
five = que5.answer;
qe.innerHTML = five;
qe.style.display = "block";
qie.style.display = "block";
} else{
  qe.style.display = "none";
  qie.style.display = "none";
}
if(wlq.f){
que6 = wlq.f;
six = que6.answer;
qf.innerHTML = six;
qf.style.display = "block";
qif.style.display = "block";
} else{
  qf.style.display = "none";
  qif.style.display = "none";
}
if(wlq.g){
que7 = wlq.g;
seven = que7.answer;
qg.innerHTML = seven;
qg.style.display = "block";
qig.style.display = "block";
} else{
  qg.style.display = "none";
  qig.style.display = "none";
}
if(wlq.h){
que8 = wlq.h;
eight = que8.answer;
qh.innerHTML = eight;
qh.style.display = "block";
qih.style.display = "block";
} else{
  qh.style.display = "none";
  qih.style.display = "none";
} 
dee = wlq.des;
one = que.answer;
two = que2.answer;
deed = dee.question;
qa.innerHTML = one;
qb.innerHTML = two;
desc.innerHTML = deed;
qrnd.innerHTML = (level) +"/15";
if(level < 2){
    qrnd.innerHTML = (level) +"/1";
} else if(level < 6) {
    qrnd.innerHTML = (level -1) +"/4";
} else{
    qrnd.innerHTML = (level -5) +"/2";
}
if(dee.double > 1){
  dblQ = dee.double;
  dbchk = true;
}
}

function backToMenu() {
  clearInterval(tutoTime);
  totalpoints = points - ((900 - timer) / 100); 
  if (typeof(Storage) !== "undefined") {
        // Store
        sessionStorage.setItem("saPoints", totalpoints);
        } else{
        alert("cant access storage");
        }
        window.location.href="menu.php";
}

function defButtons() {
  dbchk = false;
  qa.disabled = false;
  qb.disabled = false;
  qc.disabled = false;
  qd.disabled = false;
  qe.disabled = false;
  qf.disabled = false;
  qg.disabled = false;
  qh.disabled = false;
  qa.style.color = "white";
  qb.style.color = "white";
  qc.style.color = "white";
  qd.style.color = "white";
  qe.style.color = "white";
  qf.style.color = "white";
  qg.style.color = "white";
  qh.style.color = "white";
  newQ();
}

function optA() {
if(que.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qa.disabled = true;
  qa.style.color = "black";
  console.log(dblQ);
}
}
function optB() {
if(que2.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} 
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qb.disabled = true;
  qb.style.color = "black";
  console.log(dblQ);
}
}
function optC() {
if(que3.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} 
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qc.disabled = true;
  qc.style.color = "black";
}
}
function optD() {
if(que4.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qd.disabled = true;
  qd.style.color = "black";
}
}
function optE() {
if(que5.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qe.disabled = true;
  qe.style.color = "black";
}
}
function optF() {
if(que6.rw){
  points++;
} 
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qf.disabled = true;
  qf.style.color = "black";
}
}
function optG() {
if(que7.rw){
  points++;
} 
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qg.disabled = true;
  qg.style.color = "black";
}
}
function optH() {
if(que8.rw){
  points++;
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
}
  if(!end){
nextVid();
  } else {
    backToMenu();
  }
} else{
  dblQ--;
  qh.disabled = true;
  qh.style.color = "black";
}
}

</script>

</body>
</html>