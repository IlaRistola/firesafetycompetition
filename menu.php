<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>

    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Main menu</title>
    <style>
      html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
}
.toplogo {
width: 25%;
height: auto;
display: block;
margin-left: auto;
margin-right: auto;
}
.logobottom {
position: fixed;
left: 50%;
bottom: 0px;
transform: translate(-50%, -50%);
width: 10%;
height: auto;
margin: 0 auto;
}
#guide{
    position: fixed;
    bottom: 0px;
    right: 0px;
    z-index: 1;
}
.btn-group{
    z-index: 2;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: block;
}
.btn-group button {
margin: 1.5vw;
padding: 10vh;
cursor: pointer;
float: left;
border-style: hidden;
}

.strimg {
opacity:1;
position: fixed;
display: none;
z-index: 2;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
width: 512px;
height: 512px;
max-width: 100%;
max-height: 100%;
background-image: url('content/visuals/start_image.png');
background-size: 100%;
background-repeat: no-repeat;
text-align: center; 
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#yourPoints{
opacity:1;
z-index: 2;
padding: 1% 1%;
display: inline-block;
vertical-align: middle;
margin-top: 40%;
/*line-height: 500px; /* <-- adjust this */
font-style: normal;
font-weight: bold;
background-color: black;
color: white;
font-size: 40px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

.es {
opacity:1;
position: fixed;
display: none;
z-index: 2;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
width: 512px;
height: 512px;
max-width: 100%;
max-height: 100%;
background-image: url('content/visuals/end_image.png');
background-size: 100%;
background-repeat: no-repeat;
text-align: center; 
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#TBD {
opacity:1;
position: fixed;
z-index: 1;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
}
#quizPoints {
opacity:1;
display: none;
position: fixed;
z-index: 3;
top: 5%;
left: 15%;
font-size: 200%;
font-weight: bold;
border-style: solid;
background-color: lightgreen;
border-color: green;
color: navy;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

@media screen and (max-width: 579px){
.btn-group{
    left: 54%;
}
#start {
background-size: 100%;
}
#startSwe {
background-size: 100%;
}
#startEng {
background-size: 100%;
}
}
    </style>

</head>

<body>

<div id="start_test" class="strimg" onclick="startTest()"></div>
<div id="end_screen" class="es"><p id="yourPoints">FULL</p></div>

      <a-scene vr-mode-ui="enabled: false" loading-screen="enabled: false">
      <a-assets>
                <img id="stillView" preload="auto" src="content/A1_main.jpg" crossOrigin="anonymous">
                <audio id="fanfare" src="content/fanfare.mp3"></audio>
                
            </a-assets>
            <!-- HOTSPOTST -->
            <!-- 4. vaaranpaikat -->
            <a-circle class="not-touchable" id="x4" material="src: content/visuals/v4e.png" position="7.8 1 1" scale="2.2 2.2 2.2" rotation="0 270 0" opacity="0.8" visible="true" alpha-test="0.7"></a-circle>
            
            <!-- 3. liekeissa -->
            <a-circle class="not-touchable" id="x3" material="src: content/visuals/v3e.png" position="3.5 0 5" scale="2 2 2" rotation="0 200 0" opacity="0.8" visible="true" alpha-test="0.7"></a-circle>

            <!-- 2. liikenneonnettomuus -->
            <a-circle class="not-touchable" id="x2" material="src: content/visuals/v2e.png" position="-7 0.5 3" scale="2 2 2" rotation="0 120 0" opacity="0.8" alpha-test="0.7" visible="true"></a-circle>

            <!-- 1. sammutus -->
            <a-circle class="not-touchable" id="x1" material="src: content/visuals/v1e.png" alpha-test="0.7" position="-4 1 -8" scale="2.5 2.5 2.5" rotation="0 30 0" opacity="0.8" visible="true"></a-circle>

            <!-- 5. varautuminen -->
            <a-circle class="not-touchable" id="x5" material="src: content/visuals/v5e.png" alpha-test="0.7" position="5 1.1 -7" scale="2.5 2.5 2.5" rotation="0 -30 0" opacity="0.8" visible="true"></a-circle>

            <a-sky id="imgSphere" src="#stillView" rotation="0 0 0" visible="true" opacity="1" animation="property: opacity; from: 0; to: 1; dur: 500; easing: linear; startEvents: opa; loop: false" animation="property: opacity; to: 0; dur: 2000; easing: linear; startEvents: apo; loop: false">
            </a-sky>
       

            <a-entity look id="cameraWrapper" rotation="0 60 0" resetorientation>
                <a-camera look-controls mouse-cursor wasd-controls-enabled="false">
                <a-cursor
                  id="cursor"
                  animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 150"
                  animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500"
                  event-set__mouseenter="_event: mouseenter; color: springgreen"
                  event-set__mouseleave="_event: mouseleave; color: black"
                  visible="false"
                  raycaster="objects: .link">
                </a-cursor>
                <a-cursor
                id="cursor2"
                animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 150"
                animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500"
                event-set__mouseenter="_event: mouseenter; color: springgreen"
                event-set__mouseleave="_event: mouseleave; color: black"
                visible="false"
                raycaster="objects: .link2">
              </a-cursor>
              <a-cursor
              id="cursor3"
              event-set__mouseenter="_event: mouseenter;"
              event-set__mouseleave="_event: mouseleave;"
              visible="false"
              raycaster="objects: .link3">
            </a-cursor>
                    <a-entity cursor="rayOrigin: mouse" raycaster="objects: .touchable"></a-entity>
                </a-camera>
                </a-entity>
                </a-entity>

    </a-scene>


      <script>
  var menuBG = document.querySelector('#imgSphere');
  var webv1 = document.querySelector('#x1');
  var webv2 = document.querySelector('#x2');
  var webv3 = document.querySelector('#x3');
  var webv4 = document.querySelector('#x4');
  var webv5 = document.querySelector('#x5');
  var startImg = document.querySelector("#start_test");
  var totalPoints;
  var school;
  var city;
  var area;
  var team;

  /*
  Check if missions are already done.
  We use sessionStorage to track missions and user details for point collecting to database
  1. sammutus
  2. liikenneonnettomuus
  3. liekeissa 
  4. vaaranpaikat
  5. varautuminen
*/
  if (typeof(Storage) !== "undefined") {
wb1 = sessionStorage.getItem("sammutus");
wb2 = sessionStorage.getItem("liikenneonnettomuus");
wb3 = sessionStorage.getItem("liekeissa");
wb4 = sessionStorage.getItem("vaaranpaikat");
wb5 = sessionStorage.getItem("varautuminen");
email = sessionStorage.getItem("email");
school = sessionStorage.getItem("school");
city = sessionStorage.getItem("city");
area = sessionStorage.getItem("area");
team = sessionStorage.getItem("team");
pointsv1 = sessionStorage.getItem("saPoints");
pointsv2 = sessionStorage.getItem("loPoints");
pointsv3 = sessionStorage.getItem("firePoints");
pointsv4 = sessionStorage.getItem("vpPoints");
pointsv5 = sessionStorage.getItem("vaPoints");
old = sessionStorage.getItem("old");
} else {
alert("sorry, cant find session storage");
}
if(old){
    oldUser();
} else if(!wb1){
    startImg.style.display = "block";
}else if(!wb2){
    $("#x2").toggleClass("not-touchable touchable");
    webv2.setAttribute('src', 'content/visuals/v2.png');
}else if(!wb3){
    $("#x3").toggleClass("not-touchable touchable");
    webv3.setAttribute('src', 'content/visuals/v3.png');
}else if(!wb4){
    $("#x4").toggleClass("not-touchable touchable");
    webv4.setAttribute('src', 'content/visuals/v4.png');
}else if(!wb5){
    $("#x5").toggleClass("not-touchable touchable");
    webv5.setAttribute('src', 'content/visuals/v5.png');
} else{
    endPoints();
}

function oldUser() {
  
  if (confirm("Sulje selain testin päätteeksi")) {
    //window.location.href="add url"
  } else {
    //window.location.href="add url"
  }
}

function startTest() {
        startImg.style.display = "none"
        $("#x1").toggleClass("not-touchable touchable");
        webv1.setAttribute('src', 'content/visuals/v1.png');
      }
      
      function finalParty() {
      // Client ask to add some confetti & fanfare music when missions are complete.
      //confetti.start(); not added for this version
      document.querySelector("#fanfare").play();
  }

function endPoints(){
p1 = Number(pointsv1);
p2 = Number(pointsv2);
p3 = Number(pointsv3);
p4 = Number(pointsv4);
p5 = Number(pointsv5);
totalPoints = p1 + p2 + p3 + p4 + p5;
fixedPoints = (totalPoints / 270) * 100;
document.querySelector("#yourPoints").innerHTML = fixedPoints.toFixed(0) + "/100";
document.querySelector("#end_screen").style.display = "block";
send_message(email,school,city,area,team,totalPoints);
finalParty();
  if (typeof(Storage) !== "undefined") {
old = sessionStorage.setItem("old", "true");

} else {
alert("sorry, cant find session storage");
}
}

document.querySelector("#end_screen").addEventListener('mousedown', function (e) {
//window.location.href="add url"
  });
 
  webv1.addEventListener('mousedown', function (e) {
    window.location.href="sammutus.php"
  });

  webv2.addEventListener('mousedown', function (e) {
    window.location.href="liikenneonnettomuus.php"
  });

  webv3.addEventListener('mousedown', function (e) {
    window.location.href="liekeissa.php"
  });

  webv4.addEventListener('mousedown', function (e) {
    window.location.href="vaaranpaikat.php"
  });

  webv5.addEventListener('mousedown', function (e) {
    window.location.href="varautuminen.php"
  });

        function send_message(email,school,city,area,team,totalPoints) {
            $.ajax({
                type: 'POST',
                url: "scripts/insert.php",
                data: {"email":email, "school":school, "city":city, "area":area, "team":team, "totalPoints":totalPoints},
                success: function(returned) {
                    console.log(returned);
                },
                error: function(json) {
                     console.log(json);
                },
                abort: function(json) {
                }
            });
        }
       
    </script>

</body>

</html>