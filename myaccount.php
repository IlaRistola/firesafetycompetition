<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Logging details</title>
    <style>
      html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
}

.container{
    width: 50%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: rgba(187, 187, 187, 0.5);
    position: fixed;
    z-index: 2;
    }
    .headNote{
        background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 2.5vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 3%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 3%;
    }
    .form-control{
  margin-top: 1.5%;
  margin-left: 25%;
  margin-right: auto;
  margin-bottom: 1.5%;
  font-size: 2vw;
  padding: 5px 5;

    }
    .btn{
  margin-top: 1.5%;
  margin-left: 40%;
  margin-right: auto;
  margin-bottom: 3%;
  font-size: 2.5vw;
  padding: 5px 5;
  background-color: #e0101d;
  color: white;
  font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: bold;
  text-align: center;
    }

.toplogo {
width: 25%;
height: auto;
display: block;
margin-left: auto;
margin-right: auto;
}
.logobottom {
position: fixed;
left: 50%;
bottom: 0px;
transform: translate(-50%, -50%);
width: 10%;
height: auto;
margin: 0 auto;
}

    </style>

</head>

<body>
        <img src="content/visuals/logoUp.png" id="ltop" class="toplogo">
        <div class="container">
                    <?php if(!empty($error_message)) { ?>
                        <div class="alert alert-danger"><?php echo $error_message; ?></div>
                    <?php } ?>
                    <form method="post">
                        <div class="form-group" id="detailsForm" >
                            <h3 class="headNote">KOULUN TIEDOT</h3>
                            <input type="text" class="form-control" id="email" value="" placeholder="Opettajan sähköpostiosoite"><br>
                            <input type="text" class="form-control" style="display: none" id="details" value="" placeholder="Koulun nimi"><br>
                            <input type="text" class="form-control" style="display: none" id="pk" value="" placeholder="Paikkakunta"><br>
                            <select type="text" class="form-control" style="display: none" id="city" value="">
                            <option value="" id="options" disabled selected>Pelastustoimialue</option>
                            </select><br>
                            <button type="button" class="btn" onclick="getFormData()" name="submit">PAINA</button>
                            </div>
                            <div class="form-group" id="teamForm" style="display: none">
                            <h3 class="headNote">TIIMI</h3>
                            <input type="text" class="form-control" id="team" value="" placeholder=" Anna tiimille nimi"><br>
                            <button type="button" class="btn" onclick="getFormData()" name="submit">PAINA</button>
                        </div>
      </div>


      <script>
  var newText;
  var newText2;
  var newText3;
  var newTextE
  var newTextPk;

var areas = document.getElementById("city"),
Clist = ["Etelä-Karjala","Etelä-Pohjanmaa","Etelä-Savo","Helsingin kaupunki","Itä-Uusimaa","Jokilaaksot","Kainuu","Kanta-Häme","Keski-Pohjanmaa ja Pietarsaari","Keski-Suomi","Keski-Uusimaa","Kymenlaakso","Lappi","Länsi-Uusimaa","Oulu-Koillismaa","Pirkanmaa","Pohjanmaa","Pohjois-Karjala","Pohjois-Savo","Päijät-Häme","Satakunta","Varsinais-Suomi"];
for(var i = 0; i < Clist.length; i++) {
    var listOpt = document.createElement("option"),
    txt = document.createTextNode(Clist[i]);
    listOpt.appendChild(txt);
    listOpt.setAttribute("value",Clist[i]);
    areas.insertBefore(listOpt,areas.lastChild);
}

  
  function getFormData(){
  newTextE = document.getElementById('email').value;
  newText = document.getElementById('details').value;
  newTextPk = document.getElementById('pk').value;
  newText2 = document.getElementById('city').value;
  newText3 = document.getElementById('team').value;
  if(!newText){
   document.getElementById('details').style.display = "block";
   document.getElementById('email').style.display = "none";
  }
  else if(!newTextPk){
   document.getElementById('pk').style.display = "block";
   document.getElementById('details').style.display = "none";
  }
  else if(!newText2){
   document.getElementById('city').style.display = "block";
   document.getElementById('pk').style.display = "none";
  } else if(!newText3){
    document.getElementById('teamForm').style.display = "block";
    document.getElementById('detailsForm').style.display = "none";
  }
  else{
      document.getElementById('teamForm').style.display = "none";
        createUser();
  }
}

function createUser() {
if (typeof(Storage) !== "undefined") {
        // Store
        sessionStorage.setItem("email", newTextE);
        sessionStorage.setItem("school", newText);
        sessionStorage.setItem("city", newTextPk);
        sessionStorage.setItem("area", newText2);
        sessionStorage.setItem("team", newText3);
        setTimeout(function(){ 
          window.location.href="menu.php";
        }, 2000);
        } else{
        alert("cant access storage");
        }
}
    </script>

</body>

</html>