<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Liikenneonnettomuus</title>
    <style>
      html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
}
.toplogo {
width: 25%;
height: auto;
display: block;
margin-left: auto;
margin-right: auto;
}

.visa-end {
  width: 50%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  background-color: rgba(187, 187, 187, 0.5);
  padding: 20px 25px;
}

.visaEndText{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}

.itemA { grid-area: menua;}
.itemA2 { grid-area: maina; }
.itemB { grid-area: menub; }
.itemB2 { grid-area: mainb; }
.itemC { grid-area: menuc; }
.itemC2 { grid-area: mainc; }
.itemD { grid-area: menud; }
.itemD2 { grid-area: maind; }
.itemE { grid-area: menue;}
.itemE2 { grid-area: maine; }
.itemF { grid-area: menuf; }
.itemF2 { grid-area: mainf; }
.itemG { grid-area: menug; }
.itemG2 { grid-area: maing; }
.itemH { grid-area: menuh; }
.itemH2 { grid-area: mainh; }
/*.item4 { grid-area: right; }*/
.item5 { 
    grid-area: num;
    }
.item6 { grid-area: quest;
 }
.item7 { grid-area: qtext;
 }

.grid-outside {
  width: 50%;
  display: none;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: rgba(187, 187, 187, 0.5);
  position: relative;
  z-index: 2;
}
.startTestDiv {
  width: 50%;
  top: 50%;
  left: 50%;
  display: none;
  transform: translate(-50%, -50%);
  /*background-color: rgba(187, 187, 187, 0.5);*/
  background-color: #1d1d1b;
  padding:5px 5px;
  position: relative;
  z-index: 2;
}

.grid-container {
  display: grid;
  grid-template-areas:
    'menua maina maina maina maina maina'
    'menub mainb mainb mainb mainb mainb'
    'menuc mainc mainc mainc mainc mainc'
    'menud maind maind maind maind maind';
  grid-gap: 5px;
  padding: 1px;
}
.grid-container2 {
  display: grid;
  grid-template-areas:
    'menua maina maina maina maina maina'
    'menub mainb mainb mainb mainb mainb'
    'menuc mainc mainc mainc mainc mainc'
    'menud maind maind maind maind maind'
    'menue maine maine maine maine maine'
    'menuf mainf mainf mainf mainf mainf'
    'menug maing maing maing maing maing'
    'menuh mainh mainh mainh mainh mainh';
  grid-gap: 5px;
  padding: 1px;
}
.question {
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: 10vw;
  grid-template-areas:
    'num quest quest quest quest'
    'num qtext qtext qtext qtext';
  grid-gap: 0em;
  grid-column-gap: 0em;
  grid-auto-flow: dense;
  height: auto;
  margin-bottom: 10px;
}

.question .item5, .question .item7{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}
.question .item6{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-style: italic;
    background-color: #1d1d1b;
    color: #d4d3d3;
    font-size: 1vw;
}

.question > div {
  text-align: left;
  padding: 3px 20px; 
  font-size: 18px;
  height: auto;
}

.qnum {
    position: absolute;
    background-color: #e0101d;
    color: white;
  text-align: center;
  vertical-align: center;
  padding: 5px 10;
  margin-top: -10px;
  width: auto;
  font-size: 3.5vw;
  font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: bold;
  font-style: italic;
  transform-origin: top left;
  transform: skewX(-12deg);
}

.startTestDiv .startBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: center;
    padding: 10px 5;
    margin-left: 40%;
    margin-bottom: 5%;
  /*transform: translate(-50%, -50%);*/
}
.misNote{
  background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 1.2vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5%;
}
.grid-container .itemA, .grid-container .itemB, .grid-container .itemC, .grid-container .itemD{
    background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 3vw;
  padding: 3px 0;
}
.grid-container .itemA2, .grid-container .itemB2, .grid-container .itemC2, .grid-container .itemD2{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: left;
    padding: 10px 5;
}

.grid-container2 .itemA, .grid-container2 .itemB, .grid-container2 .itemC, .grid-container2 .itemD, .grid-container2 .itemE, .grid-container2 .itemF, .grid-container2 .itemG, .grid-container2 .itemH{
    background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 3vw;
  padding: 3px 0;
}
.grid-container2 .itemA2, .grid-container2 .itemB2, .grid-container2 .itemC2, .grid-container2 .itemD2, .grid-container2 .itemE2, .grid-container2 .itemF2, .grid-container2 .itemG2, .grid-container2 .itemH2{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: left;
    padding: 10px 5;
    outline: none;
}

.btn-group{
    z-index: 2;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: block;
}
.btn-group button {
margin: 1.5vw;
padding: 10vh;
cursor: pointer;
float: left;
border-style: hidden;
}


#TBD {
opacity:1;
position: fixed;
z-index: 1;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
}
#quizPoints {
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

#gameTime {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

#videoKierros {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 82%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

.queImage{
  width: 850px;
  height: 650px;
  display: none;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: rgba(187, 187, 187, 0.5);
  position: relative;
  z-index: 2;
}

.qimg {
/*height: 480px;
width: 720px;*/
margin-left: auto;
margin-right: auto;
margin-top: 5%;
margin-bottom: 2%;
/*position: relative;*/
/*padding-top: 5%;
padding-bottom: 1%;*/
}

.picBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 2.5vw;
    padding: 10px;
    /*text-align: center;*/
    /*position: relative;*/
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 2%;
    margin-top: 2%;
}

    </style>

<!--script src="scripts/regcomp2.js"></script-->

</head>

<body>

  <p id="gameTime"></p>
  <p id="videoKierros"></p>
  <div class="startTestDiv" id="startDivi">
    <div class="missionGuide"><div class="misNote" id="thisNote">Tehtävänämme on valita mielestänne oikea järjestys toimia onnettomuustilanteessa. Lukekaa kysymykset tarkkaan, sillä tehtyä valintaa EI voi muuttaa.<br>Mopoilija on kaatunut liukkaalla tiellä ja istuu maassa. Hän on lyönyt päänsä ja valittaa kipua olkapäässä. Toisessa jalassa on iso haava, toisessa jalassa pakoputkesta aiheutunut palovamma. Miten tilanteessa tulee toimia?"</div></div>
    <button class="startBut" id="stbut" onclick="startTest()">Aloita testi</button>
  </div>
  <div class="queImage" id="questImg" style="vertical-align:middle; text-align:center">
    <img class="qimg" id="qimg" src="content/visuals/liikenneonnettomuus/a1s.jpg">
     <button class="picBut" id="picbut" onclick="nextOne()">JATKA</button>
  </div>
  <div class="grid-outside" id="gOutside">
    <div class="qnum" id="qRound">1/6</div>
      <div class="question"><div class="item5"></div><div class="item6">Tehtävänanto</div><div class="item7" id="description">Kotkot</div></div>
      <!-- missions questions coming from json list -->
    <div class="grid-container2">
      <div class="itemA">A</div>
      <button class="itemA2" id="ia2" onclick="optA()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemB">B</div>
      <button class="itemB2" id="ib2" onclick="optB()">Pysähdyn paikalle kysymään, voinko auttaa jotenkin.</button> 
      <div class="itemC" id="ic" style="display: none">C</div>
      <button class="itemC2" id="ic2" onclick="optC()" style="display: none">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button> 
      <div class="itemD" id="id" style="display: none">D</div>
      <button class="itemD2" id="id2" onclick="optD()" style="display: none">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemE" id="ie" style="display: none">E</div>
      <button class="itemE2" style="display: none" id="ie2" onclick="optE()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>
      <div class="itemF" id="if" style="display: none">F</div>
      <button class="itemF2" style="display: none" id="if2" onclick="optF()">Pysähdyn paikalle kysymään, voinko auttaa jotenkin.</button> 
      <div class="itemG" id="ig" style="display: none">G</div>
      <button class="itemG2" style="display: none" id="ig2" onclick="optG()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button> 
      <div class="itemH" id="ih" style="display: none">H</div>
      <button class="itemH2" style="display: none" id="ih2" onclick="optH()">Jatkat matkaasi kouluun, koska paikalla on muita auttajia.</button>    
    </div>
  </div>

<script src="scripts/liikenneonnettomuus.js"></script>

</body>
</html>