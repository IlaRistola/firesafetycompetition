
      
      // created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //

  a = false;
  var x;
  var tank = 100;
  var i;
  var takeTime;
  var tutoTime;
  var fireTime;
  var playTime;
  var time = 91;
  f1on = false;
  f1_2on = false;
  f2on = false;
  f2_2on = false;
  fton = false;
  fiEx = document.querySelector("#fireEx");
  foamShady = document.querySelector("#foamSphere");
  fi = document.querySelector("#fire");
  fi1 = document.querySelector("#fire1");
  fi1_2 = document.querySelector("#fire1_2");
  fi2 = document.querySelector("#fire2");
  fi2_2 = document.querySelector("#fire2_2");
  foam1 = document.querySelector("#foam1");
  foam1_2 = document.querySelector("#foam1_2");
  foam2 = document.querySelector("#foam2");
  foam2_2 = document.querySelector("#foam2_2");
  timeText = document.getElementById("gameTime");
  tutorialImg = document.querySelector("#startTutorial");
  startTest = document.querySelector("#startTest");
  foamBar = document.querySelector("#myBar");
  tankBase = document.querySelector("#myProgress");
  gL = document.querySelector("#guideLayer");
  guidVid = document.querySelector("#guide");
  tutoVid = document.querySelector("#tutorialVideo");
  tutoLayer = document.querySelector("#tutorialLayer");
  back = document.querySelector("#back");
  iSp = document.querySelector("#imgSphere");
  var endBurn = false;
  cam = document.querySelector("#cameraWrapper");
  var player = 1;
  var totalPoints = 0;
  
  
  document.querySelector('a-scene').addEventListener('loaded', function () {
    if (typeof(Storage) !== "undefined") {
    // Store
    game = sessionStorage.getItem("liekeissa");
    if(game){
      window.location.href="menu.php";
    } else{
        sessionStorage.setItem("liekeissa", "true");
      }
    } else{
    alert("cant access storage");
    }
  
  tutorialImg.style.display = "block"
  });
  
  function startTutorial() {
    tutorialImg.style.display = "none"
    tutoVid.play();
    tutoLayer.setAttribute("visible", true);
  }
  
  tutoVid.addEventListener('ended', function(e) {
  tutoLayer.setAttribute("visible", false);
      if(!tutoVid.paused){
      tutoVid.pause();
      }
      fi.setAttribute("visible", true);
      fiEx.setAttribute("visible", true);
      timeText.style.display = "block";
      tankBase.style.display = "block";
      tutorial();
  });
  
  /* fire extinguisher training level */
  function tutorial() {
  fton = true;
  document.querySelector("#tutorialTargets").setAttribute("visible", true);
  tutorialFire();
  document.querySelector("#cursor").setAttribute("visible", true);
  if(fiEx.matches('.not-touchable')){
  $("#fireEx").toggleClass("not-touchable touchable");
  }
  }
  
  function tankTutorial() {
  if(fton){
  tutoTime = setInterval(function(){ 
  if(a){ 
  s = flameSpot.getAttribute("scale");
  c = flameSpot.getAttribute("position");
  cx = c.x;
  cz = c.z;
  cy = c.y;
  sx = s.x
  sy = s.y;
  if(flameSpot.id == "fire" && fton){
  if(power == 1){sx = s.x -0.70;sy = s.y -0.70;cy = c.y - 0.266;}
  else if(power == 2){sx = s.x -0.35;sy = s.y -0.35;cy = c.y - 0.133;}
  else{sx = s.x -0.25;sy = s.y -0.25;cy = c.y - 0.095;}
  }
  flameSpot.setAttribute('scale', {x: sx, y:sy});
  flameSpot.setAttribute('position', {x: cx, y: cy, z: cz});
  }
  }, 500);
  } else{
  clearInterval(tutoTime);
  }
  }
  
  /* let's create fire for training level which not spread */
  function tutorialFire() {
  fireTime = setInterval(function(){
    fireUp = fi.getAttribute("scale");
    firePos = fi.getAttribute("position");
  
    if(fireUp.x < 15 && fireUp.x > 4){
    fx = fireUp.x +0.2;
    fy = fireUp.y +0.2;
    fpx = firePos.x;
    fpy = firePos.y + 0.076;
    fpz = firePos.z;
    fi.setAttribute("scale", {x: fx, y: fy});
    fi.setAttribute("position", {x: fpx, y: fpy, z: fpz});
    } else if(fireUp.x <= 4){
      fi.setAttribute("visible", false);
      document.querySelector("#tutorialTargets").setAttribute("visible", false);
      $("#T1").removeClass("link"),$("#T2").removeClass("link"),$("#T3").removeClass("link");
      fton = false;
      document.querySelector("#cursor").setAttribute("visible", false);
      if(fiEx.matches('.touchable')){
      $("#fireEx").toggleClass("touchable not-touchable");
      r = cam.getAttribute('rotation');
      rx = r.x;
      ry = r.y + 160;
      rz = r.z;
      cam.setAttribute('rotation', {x: rx, y: ry, z: rz});
      foamShady.setAttribute("visible", false);
      startTest.style.display = "block";
      timeText.style.display = "none";
      timeText.innerHTML = "90";
      clearInterval(fireTime);
      }
    }
    
  }, 500);
  }
  
  function startChallenge() {
  f1on = true;
  f1_2on = false;
  f2on = true;
  f2_2on = false;
  tank = 100;
  fi1.setAttribute("visible", true),
  fi1_2.setAttribute("visible", true),
  fi2.setAttribute("visible", true),
  fi2_2.setAttribute("visible", true);
  $("fire1").addClass("link"),
  $("fire1_2").addClass("link"),
  $("fire2").addClass("link"),
  $("fire2_2").addClass("link");
  document.querySelector("#cursor").setAttribute("visible", true);
  startTest.style.display = "none";
  fiEx.setAttribute("visible", true);
  if(fiEx.matches('.not-touchable')){
  $("#fireEx").toggleClass("not-touchable touchable");
  }
    burning(); // Create 2 fire objective that start spread and create 2 fire objective more.
    tankBar(); // Controlling how much foam is left on tank. When foam end it's game over.
    Timer(); // Play time. All fires need to shut down before times end.
    iSp.emit("opa");
    timeText.style.display = "block";
  }
  
  function newBurning() {
  /* This is created for reset burnings and callect round points after first play round (totally three rounnds) */
  console.log(totalPoints);
  console.log(player);
  if(player < 4){
    f1on = true;
  f1_2on = false;
  f2on = true;
  f2_2on = false;
  tank = 100;
  endBurn = false;
    fi1.setAttribute("position", {x:-11, y:0, z:0.5});
    fi1.setAttribute("scale", {x:5, y:5}),
    fi1_2.setAttribute("position", {x:-9, y:-1, z:2}),
    fi1_2.setAttribute("scale", {x:2, y:1}),
    fi1_2.setAttribute("opacity", 0),
    fi2.setAttribute("position", {x:3, y:0.2, z:2.7}),
    fi2.setAttribute("scale", {x:1, y:2}),
    fi2_2.setAttribute("position", {x:4, y:0.16, z:8}),
    fi2_2.setAttribute("scale", {x:2, y:2}),
    fi2_2.setAttribute("opacity", 0);
    foam1.setAttribute("visible", false),
    foam1_2.setAttribute("visible", false),
    foam2.setAttribute("visible", false),
    foam2_2.setAttribute("visible", false);
    fi1.setAttribute("visible", true),
    fi1_2.setAttribute("visible", true),
    fi2.setAttribute("visible", true),
    fi2_2.setAttribute("visible", true);
    document.querySelector("#endScreen").style.display = "none";
    endBurn = false;
    burning();
    tankBar();
    Timer();
    iSp.emit("opa");
  } else{
    if (typeof(Storage) !== "undefined") {
    // Store
    sessionStorage.setItem("firePoints", totalPoints);
    } else{
    alert("cant access storage");
    }
    window.location.href="menu.php";
  }
  };
  
  document.querySelector("#cursor").addEventListener('mouseenter', function (e) {
      a = true;
  });
  document.querySelector("#cursor").addEventListener('mouseleave', function (e) {
      a = false;
  });
  
  $(document).mouseleave(function () {
   if(fton){
     clearInterval(tutoTime);
   } else{
   clearInterval(takeTime);
   }
   foamShady.setAttribute("visible", false);
  });
  
  fiEx.addEventListener('mousedown', function (e) { 
      if(fton){
        tankTutorial();
      } else{
      tankTime();
      }
      foamShady.setAttribute("visible", true);
  });
  fiEx.addEventListener('mouseup', function (e) { 
    if(fton){
      clearInterval(tutoTime);
    } else{
    clearInterval(takeTime);
    }
    foamShady.setAttribute("visible", false);
  });
  
  
  function Timer() {
    time = 91;
  playTime = setInterval(function(){ 
  time--;
  timeText.innerHTML = time;
  if(time == 0){ 
  timeText.innerHTML = "0";
  clearInterval(playTime);
  pointsCalculator();
  } 
  else if (!f1on&&!f1_2on&&!f2on&&!f2_2on){
  clearInterval(playTime);
  pointsCalculator();
  } else if(tank == 0){
  clearInterval(playTime);
  pointsCalculator();
    } 
  }, 1000);
  }
  
  /* for calculating points after game rounds */
  function pointsCalculator() {
  foamShady.setAttribute("visible", false);
  endBurn = true;
  iSp.emit("apo");
  x = 0;
  var points;
  var failPoints;
  if(f1on){x++;}if(f1_2on){x++;}if(f2on){x++;}if(f2_2on){x++;}
  if(x > 0 && x < 4){
  points = ((time) + (tank) + (x * 12.50)) / 3;  // points calculating fixed with number of time, tank usage and number of fire shutdowns
  document.querySelector("#yourPoints").innerHTML = points.toFixed(2);
  totalPoints = totalPoints + points;
  player++;
  if(player > 3){
  document.querySelector("#endScreen").style.backgroundImage = "url('content/visuals/Game/f4b.png')";
  document.querySelector("#endScreen").style.display = "block";
  } else{
  document.querySelector("#endScreen").style.display = "block";
  }
  } else if(x == 4){
    points = 0;
  document.querySelector("#yourPoints").innerHTML = 0;
  totalPoints = totalPoints + points;
  player++;
  if(player > 3){
  document.querySelector("#endScreen").style.backgroundImage = "url('content/visuals/Game/f4b.png')";
  document.querySelector("#endScreen").style.display = "block";
  } else{
  document.querySelector("#endScreen").style.display = "block";
  }
  }
  else {
  points = ((time) + (tank) + 50) / 3;
  document.querySelector("#yourPoints").innerHTML = points.toFixed(2);
  totalPoints = totalPoints + points;
  player++;
  if(player > 3){
  document.querySelector("#endScreen").style.backgroundImage = "url('content/visuals/Game/f4b.png')";
  document.querySelector("#endScreen").style.display = "block";
  } else{
  document.querySelector("#endScreen").style.display = "block";
  }
  }
  }
  
  function tankTime() {
  // let's use tank usage 4 on final. Original 1.25
    if(tank < 0){
    tank -= 4; 
    } 
  takeTime = setInterval(function(){ 
  if (tank == 0) {
  clearInterval(takeTime);
  } else if(endBurn){
  clearInterval(takeTime);
  } else {
  tankBar();
  tank -= 4;
  if(a){
  s = flameSpot.getAttribute("scale");
  c = flameSpot.getAttribute("position");
  cx = c.x;
  cz = c.z;
  cy = c.y;
  sx = s.x
  sy = s.y;
  if(flameSpot.id == "fire2" && f2on){
  if(power == 1){sx = s.x -0.175;sy = s.y -0.70;cy = c.y - 0.199;}
  else if(power == 2){sx = s.x -0.125;sy = s.y -0.50;cy = c.y - 0.14;}
  else{sx = s.x -0.05;sy = s.y -0.2;cy = c.y - 0.067;}
  }
  else if(flameSpot.id == "fire2_2" && f2_2on){
  if(power == 1){sx = s.x -0.35;sy = s.y -0.70;cy = c.y - 0.266;}
  else if(power == 2){sx = s.x -0.25;sy = s.y -0.50;cy = c.y - 0.19;}
  else{sx = s.x -0.1;sy = s.y -0.2;cy = c.y - 0.076;}
  } else if(flameSpot.id == "fire1" && f1on){
  if(power == 1){sx = s.x -0.70;sy = s.y -0.70;cy = c.y - 0.266;}
  else if(power == 2){sx = s.x -0.25;sy = s.y -0.25;cy = c.y - 0.095;}
  else{sx = s.x -0.1;sy = s.y -0.1;cy = c.y - 0.038;}
  } else if(flameSpot.id == "fire1_2" && f1_2on){
  if(power == 1){sx = s.x -0.70;sy = s.y -0.70;cy = c.y - 0.266;}
  else if(power == 2){sx = s.x -0.25;sy = s.y -0.25;cy = c.y - 0.095;}
  else{sx = s.x -0.1;sy = s.y -0.1;cy = c.y - 0.038;}
  }
  flameSpot.setAttribute('scale', {x: sx, y:sy});
  flameSpot.setAttribute('position', {x: cx, y: cy, z: cz});
  }
  }
  }, 500);
  }
  
  function burning() {
  /* creating the burnings. Fire objective size is pre-choosen. And we re-size between time to highest limit. */
  fireTime = setInterval(function(){
    fire1Up = fi1.getAttribute("scale");
    fire1Pos = fi1.getAttribute("position");
    fire1_2Up = fi1_2.getAttribute("scale");
    fire1_2Pos = fi1_2.getAttribute("position");
    fire2Up = fi2.getAttribute("scale");
    fire2Pos = fi2.getAttribute("position");
    fire2_2Up = fi2_2.getAttribute("scale");
    fire2_2Pos = fi2_2.getAttribute("position");
  
    if(fire1Up.x < 11 && fire1Up.x > 2){
    fx = fire1Up.x +0.05;
    fy = fire1Up.y +0.05;
    fpx = fire1Pos.x;
    fpy = fire1Pos.y + 0.019;
    fpz = fire1Pos.z;
    fi1.setAttribute("scale", {x: fx, y: fy});
    fi1.setAttribute("position", {x: fpx, y: fpy, z: fpz});
    } else if(fire1Up.x <= 2){
      fi1.setAttribute("visible", false);
      foam1.setAttribute("visible", true);
      f1on = false;
    }
    if(fire1_2Up.x <= 1){
      fi1_2.setAttribute("visible", false);
      foam1_2.setAttribute("visible", true);
      f1_2on = false;
  }
   else if(fire1Up.x > 5.5 && fire1_2Up.x < 8){
     /* we wake another fires if first one is too strong to give spread effect for it */
    f1_2on = true;
    fi1_2.emit("opa");
    fx = fire1_2Up.x +0.05;
    fy = fire1_2Up.y +0.05;
    fpx = fire1_2Pos.x;
    fpy = fire1_2Pos.y + 0.019;
    fpz = fire1_2Pos.z;
    fi1_2.setAttribute("scale", {x: fx, y: fy});
    fi1_2.setAttribute("position", {x: fpx, y: fpy, z: fpz});
    } 
  
    if(fire2Up.y < 6 && fire2Up.y > 0.5){
    if(fire2Up.x < 2){
    fx = fire2Up.x +0.05;
    } else{
      fx = fire2Up.x;
    }
    fy = fire2Up.y +0.05;
    fpx = fire2Pos.x;
    fpy = fire2Pos.y + 0.019;
    fpz = fire2Pos.z;
    fi2.setAttribute("scale", {x: fx, y: fy});
    fi2.setAttribute("position", {x: fpx, y: fpy, z: fpz});
    } else if(fire2Up.y <= 0.5){
      fi2.setAttribute("visible", false);
      foam2.setAttribute("visible", true);
      f2on = false;
    }
    if(fire2_2Up.y <= 1.9){
      fi2_2.setAttribute("visible", false);
      foam2_2.setAttribute("visible", true);
      f2_2on = false;
    }
    else if(fire2Up.y > 3 && fire2_2Up.x < 10){
    f2_2on = true;
    fi2_2.emit("opa");
    fx = fire2_2Up.x +0.1;
    fy = fire2_2Up.y +0.15;
    fpx = fire2_2Pos.x;
    fpy = fire2_2Pos.y + 0.057;
    fpz = fire2_2Pos.z;
    fi2_2.setAttribute("scale", {x: fx, y: fy});
    fi2_2.setAttribute("position", {x: fpx, y: fpy, z: fpz});
    }
  
    if(endBurn){
      clearInterval(fireTime);
    }
  }, 500);
  }
  
  function tankBar() {
    var elem = foamBar;
    var height = tank;
  elem.style.height = height + "%";
  }