
      // created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //

  if (typeof(Storage) !== "undefined") {
    // Store
    game = sessionStorage.getItem("vaaranpaikat");
    if(game){
      window.location.href="menu.php";
    } else{
        sessionStorage.setItem("vaaranpaikat", "true");
      }
    } else{
    alert("cant access storage");
    }

iSp = document.querySelector("#imgSphere");
hsG1 = document.querySelector("#hsGroup1");
camImage = document.querySelector("#quiz");
cam = document.querySelector("#cameraWrapper");
sDiv = document.getElementById("startDivi");
var timer = 180;
totalpoints = 0;
var playTime = document.getElementById("gameTime");
qp = document.querySelector("#quizPoints");

function dangerPlaces(){
sDiv.style.display = "none";
playTime.style.display = "block";
Clock();

$("#hotspot1").toggleClass("not-touchable touchable"),
$("#hotspot2").toggleClass("not-touchable touchable"),
$("#hotspot3").toggleClass("not-touchable touchable"),
$("#hotspot4").toggleClass("not-touchable touchable"),
$("#hotspot5").toggleClass("not-touchable touchable"),
$("#hotspot6").toggleClass("not-touchable touchable");
$("#hotspot7").toggleClass("not-touchable touchable");
qPoints = 0;
h1 = false;
h2 = false;
h3 = false;
h4 = false;
h5 = false;
h6 = false;
h7 = false;
qp.innerHTML = qPoints + " / 7"
qp.style.display = "block";
}

function Clock(duration, display){
var timing = duration, minutes, seconds;
tutoTime = setInterval(function(){ 
timer--;
minutes = parseInt(timer / 60, 10);
seconds = parseInt(timer % 60, 10);
minutes = minutes < 10 ? "0" + minutes : minutes;
seconds = seconds < 10 ? "0" + seconds : seconds;
playTime.innerHTML = minutes + ":" + seconds;
if(timer == 0){
clearInterval(tutoTime); 
backToMenu();
}
}, 1000);
}

/* There is 7 different hotspots that user need to find from 360 view */
document.querySelector("#hotspot1").addEventListener('mousedown', function (e) {
h1 = true;
qPoints++;
$("#hotspot1").toggleClass("touchable not-touchable");
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p1");
});
document.querySelector("#hotspot4").addEventListener('mousedown', function (e) {
h4 = true;
qPoints++;
$("#hotspot4").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p4");
});
document.querySelector("#hotspot2").addEventListener('mousedown', function (e) {
h2 = true;
qPoints++;
$("#hotspot2").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable"); }, 1000);
camImage.setAttribute("src", "#p2");
});
document.querySelector("#hotspot5").addEventListener('mousedown', function (e) {
h5 = true;
qPoints++;
$("#hotspot5").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p5");
});
document.querySelector("#hotspot3").addEventListener('mousedown', function (e) {
h3 = true;
qPoints++;
$("#hotspot3").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p3");
});
document.querySelector("#hotspot6").addEventListener('mousedown', function (e) {
h6 = true;
qPoints++;
$("#hotspot6").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h7){$("#hotspot7").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p6");
});
document.querySelector("#hotspot7").addEventListener('mousedown', function (e) {
h7 = true;
qPoints++;
$("#hotspot7").toggleClass("touchable not-touchable");
if(!h1){$("#hotspot1").toggleClass("touchable not-touchable")}
if(!h2){$("#hotspot2").toggleClass("touchable not-touchable")}
if(!h3){$("#hotspot3").toggleClass("touchable not-touchable")}
if(!h4){$("#hotspot4").toggleClass("touchable not-touchable")}
if(!h5){$("#hotspot5").toggleClass("touchable not-touchable")}
if(!h6){$("#hotspot6").toggleClass("touchable not-touchable")}
setTimeout(function(){ qp.innerHTML = qPoints + " / 7"; camImage.setAttribute("visible", true); camImage.emit("ani"); $("#quiz").toggleClass("not-touchable touchable");}, 1000);
camImage.setAttribute("src", "#p7");
});

camImage.addEventListener('mousedown', function (e) {
$("#quiz").toggleClass("touchable not-touchable");
camImage.setAttribute("visible", false);

setTimeout(function(){
if(qPoints == 7){
qPoints++;
clearInterval(tutoTime); 
camImage.setAttribute("src", "#end");
$("#quiz").toggleClass("not-touchable touchable");
camImage.setAttribute("visible", true);
camImage.emit("ani");
}
else if(qPoints == 8){
backToMenu();
} else {
if(!h1){ $("#hotspot1").toggleClass("not-touchable touchable");}
if(!h2){ $("#hotspot2").toggleClass("not-touchable touchable");}
if(!h3){ $("#hotspot3").toggleClass("not-touchable touchable");}
if(!h4){ $("#hotspot4").toggleClass("not-touchable touchable");}
if(!h5){ $("#hotspot5").toggleClass("not-touchable touchable");}
if(!h6){ $("#hotspot6").toggleClass("not-touchable touchable");}
if(!h7){ $("#hotspot7").toggleClass("not-touchable touchable");}
} 
}, 1000);
});

function backToMenu() {
//collect points and move user back to menu screen
if (typeof(Storage) !== "undefined") {
totalpoints = qPoints - ((120 - timer) / 100);
    // Store
    sessionStorage.setItem("vpPoints", totalpoints);
    } else{
    alert("cant access storage");
    }
    window.location.href="menu.php";
}