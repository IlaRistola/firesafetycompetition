      // created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //

  if (typeof(Storage) !== "undefined") {
    // Store
    game = sessionStorage.getItem("varautuminen");
    if(game){
      window.location.href="menu.php";
    } else{
        sessionStorage.setItem("varautuminen", "true");
      }
    } else{
    alert("cant access storage");
    }

qa = document.getElementById("ia2");
qb = document.getElementById("ib2");
qc = document.getElementById("ic2");
qd = document.getElementById("id2");
qe = document.getElementById("ie2");
qf = document.getElementById("if2");
qg = document.getElementById("ig2");
qh = document.getElementById("ih2");
qi = document.getElementById("ii2");
qj = document.getElementById("ij2");
qk = document.getElementById("ik2");
ql = document.getElementById("il2");
qm = document.getElementById("im2");
qn = document.getElementById("in2");
qo = document.getElementById("io2");
qp = document.getElementById("ip2");
qq = document.getElementById("iq2");
qr = document.getElementById("ir2");
qs = document.getElementById("is2");
qt = document.getElementById("it2");
qu = document.getElementById("iu2");
qv = document.getElementById("iv2");
desc = document.getElementById("description");
qrnd = document.getElementById("qRound");

sDiv = document.getElementById("startDivi");
qGrid = document.getElementById("gOutside");
var qRounder = 0;
var points = 0;
var timer = 300;
totalPoints = 0;
var playTime = document.getElementById("gameTime");


function startTest() {
sDiv.style.display = "none";
qGrid.style.display = "block";
playTime.style.display = "block";
Clock();
}

function Clock(duration, display){
var timing = duration, minutes, seconds;
tutoTime = setInterval(function(){ 
timer--;
minutes = parseInt(timer / 60, 10);
seconds = parseInt(timer % 60, 10);
minutes = minutes < 10 ? "0" + minutes : minutes;
seconds = seconds < 10 ? "0" + seconds : seconds;
playTime.innerHTML = minutes + ":" + seconds;
if(timer == 0){
backToMenu();
}
}, 1000);
}

// Handle the game rounds
function newQ() {
if(qRounder < 6){
qRounder++;
qrnd.innerHTML = qRounder + "/7";
} else {
qRounder++;
qrnd.innerHTML = qRounder + "/7";
backToMenu();
}
}

function backToMenu() {
// collect points and move user back to main menu
clearInterval(tutoTime);
totalpoints = points - ((300 - timer) / 100);  //check original time!
if (typeof(Storage) !== "undefined") {
    // Store
    sessionStorage.setItem("vaPoints", totalpoints);
    } else{
    alert("cant access storage");
    }
    window.location.href="menu.php";
}

function defButtons() {
dbchk = false;
qa.disabled = false;
qb.disabled = false;
qc.disabled = false;
qd.disabled = false;
qa.style.color = "white";
qb.style.color = "white";
qc.style.color = "white";
qd.style.color = "white";
newQ();
}

function optA() {
points++;
qa.disabled = true;
qa.style.opacity = "0.5";
newQ();
}
function optB() {
qb.disabled = true;
qb.style.opacity = "0.5";
newQ();
}
function optC() {
qc.disabled = true;
qc.style.opacity = "0.5";
newQ();
}
function optD() {
qd.disabled = true;
qd.style.opacity = "0.5";
newQ();
}
function optE() {
qe.disabled = true;
qe.style.opacity = "0.5";
newQ();
}
function optF() {
points++;
qf.disabled = true;
qf.style.opacity = "0.5";
newQ();
}
function optG() {
qg.disabled = true;
qg.style.opacity = "0.5";
newQ();
}
function optH() {
qh.disabled = true;
qh.style.opacity = "0.5";
newQ();
}
function optI() {
points++;
qi.disabled = true;
qi.style.opacity = "0.5";
newQ();
}
function optJ() {
qj.disabled = true;
qj.style.opacity = "0.5";
newQ();
}
function optK() {
qk.disabled = true;
qk.style.opacity = "0.5";
newQ();
}
function optL() {
ql.disabled = true;
ql.style.opacity = "0.5";
newQ();
}
function optM() {
points++;
qm.disabled = true;
qm.style.opacity = "0.5";
newQ();
}
function optN() {
points++;
qn.disabled = true;
qn.style.opacity = "0.5";
newQ();
}
function optO() {
qo.disabled = true;
qo.style.opacity = "0.5";
newQ();
}
function optP() {
qp.disabled = true;
qp.style.opacity = "0.5";
newQ();
}
function optQ() {
points++;
qq.disabled = true;
qq.style.opacity = "0.5";
newQ();
}
function optR() {
qr.disabled = true;
qr.style.opacity = "0.5";
newQ();
}
function optS() {
qs.disabled = true;
qs.style.opacity = "0.5";
newQ();
}
function optT() {
qt.disabled = true;
qt.style.opacity = "0.5";
newQ();
}
function optU() {
points++;
qu.disabled = true;
qu.style.opacity = "0.5";
newQ();
}
function optV() {
qv.disabled = true;
qv.style.opacity = "0.5";
newQ();
}

