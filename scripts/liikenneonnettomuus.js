  // created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //
  
  var json = (function() {
    // Real question list is hidden from public in gitlab
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': "jsons/lotwo.json",
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();

if (typeof(Storage) !== "undefined") {
        // Store
        game = sessionStorage.getItem("liikenneonnettomuus");
        if(game){
          window.location.href="menu.php";
        } else{
            sessionStorage.setItem("liikenneonnettomuus", "true");
          }
        } else{
        alert("cant access storage");
        }

qa = document.getElementById("ia2");
qb = document.getElementById("ib2");
qc = document.getElementById("ic2");
qic = document.getElementById("ic");
qd = document.getElementById("id2");
qid = document.getElementById("id");
qe = document.getElementById("ie2");
qie = document.getElementById("ie");
qf = document.getElementById("if2");
qif = document.getElementById("if");
qg = document.getElementById("ig2");
qig = document.getElementById("ig");
qh = document.getElementById("ih2");
qih = document.getElementById("ih");
desc = document.getElementById("description");
qrnd = document.getElementById("qRound");
vidSph = document.querySelector("#vidSphere");
vid360 = document.querySelector("#videoview");
vid3602 = document.querySelector("#videoview2");
vid3603 = document.querySelector("#videoview3");
sDiv = document.getElementById("startDivi");
qGrid = document.getElementById("gOutside");
qImgGrid = document.getElementById("questImg");
qpic = document.getElementById("qimg");
var wl = json;
var qlist = [wl.q1,wl.q2,wl.q3,wl.q4,wl.q5,wl.q6,wl.q7,wl.q8,wl.q9,wl.q10,wl.q11,wl.q12,wl.q13,wl.q14];
var q = 0;
var wlq = qlist[q];
var que = wlq.a;
var que2 = wlq.b;
var que3 = wlq.c;
var que4 = wlq.d;
var que5 = wlq.e;
var que6 = wlq.f;
var que7 = wlq.g;
var que8 = wlq.h;
var dee = wlq.des;
var one = que.answer;
var two = que2.answer;
var five;
var six;
var seven;
var eigth;
var deed = dee.question;
var qRounder = 0;
var points = 0;
var dblQ = 1;
var dbchk = false;
var level = 1;
var qRoundLimit = 1;
var timer = 600;
totalPoints = 0;
var playTime = document.getElementById("gameTime");
var videoRd = document.getElementById("videoKierros");
var end = false;

  sDiv.style.display = "block";

function Clock(duration, display){
var timing = duration, minutes, seconds;
tutoTime = setInterval(function(){ 
timer--;
minutes = parseInt(timer / 60, 10);
seconds = parseInt(timer % 60, 10);
minutes = minutes < 10 ? "0" + minutes : minutes;
seconds = seconds < 10 ? "0" + seconds : seconds;
playTime.innerHTML = minutes + ":" + seconds;
if(timer == 0){
  backToMenu();
}
}, 1000);
}

function startTest() {
  sDiv.style.display = "none";
  qImgGrid.style.display = "block";
  playTime.style.display = "block";
  Clock();
}

function nextOne() {
  if(level < 9){
  qRounder = 0;
  qImgGrid.style.display = "none";
  qGrid.style.display = "block";
  level++;
  newQ();
  imageLoader();
}
}

function imageLoader() {
  if(level == 2){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a2s.jpg');
  } else if(level == 3){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a3s.jpg');
  } else if(level == 4){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a4s.jpg');
  } else if(level == 5){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a5s.jpg');
  } else if(level == 6){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a6s.jpg');
  } else if(level == 7){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a7s.jpg');
  } else if(level == 8){
    qpic.setAttribute('src', 'content/visuals/liikenneonnettomuus/a8s.jpg');
  } else if(level == 9){
      end = true;
  }
}

function newQ() {
if(qRounder < qRoundLimit){
wlq = qlist[q];
que = wlq.a;
que2 = wlq.b;
if(wlq.c){
que3 = wlq.c;
three = que3.answer;
qc.innerHTML = three;
qc.style.display = "block";
qic.style.display = "block";
} else{
  qc.style.display = "none";
  qic.style.display = "none";
}
if(wlq.d){
que4 = wlq.d;
four = que4.answer;
qd.innerHTML = four;
qd.style.display = "block";
qid.style.display = "block";
} else{
  qd.style.display = "none";
  qid.style.display = "none";
}
if(wlq.e){
que5 = wlq.e;
five = que5.answer;
qe.innerHTML = five;
qe.style.display = "block";
qie.style.display = "block";
} else{
  qe.style.display = "none";
  qie.style.display = "none";
}
if(wlq.f){
que6 = wlq.f;
six = que6.answer;
qf.innerHTML = six;
qf.style.display = "block";
qif.style.display = "block";
} else{
  qf.style.display = "none";
  qif.style.display = "none";
}
if(wlq.g){
que7 = wlq.g;
seven = que7.answer;
qg.innerHTML = seven;
qg.style.display = "block";
qig.style.display = "block";
} else{
  qg.style.display = "none";
  qig.style.display = "none";
}
if(wlq.h){
que8 = wlq.h;
eight = que8.answer;
qh.innerHTML = eight;
qh.style.display = "block";
qih.style.display = "block";
} else{
  qh.style.display = "none";
  qih.style.display = "none";
} 
dee = wlq.des;
one = que.answer;
two = que2.answer;
deed = dee.question;
qa.innerHTML = one;
qb.innerHTML = two;
desc.innerHTML = deed;
qRounder++;
qrnd.innerHTML = (level - 1) +"/8";
if(dee.double > 1){
  dblQ = dee.double;
  dbchk = true;
}
}
  else{
  qGrid.style.display = "none";
  qImgGrid.style.display = "block";
}
}

function backToMenu() {
  clearInterval(tutoTime);
  totalpoints = points - ((600 - timer) / 100);
  if (typeof(Storage) !== "undefined") {
        // Store
        sessionStorage.setItem("loPoints", totalpoints);
        } else{
        alert("cant access storage");
        }
        window.location.href="menu.php";
}

function defButtons() {
  dbchk = false;
  qa.disabled = false;
  qb.disabled = false;
  qc.disabled = false;
  qd.disabled = false;
  qe.disabled = false;
  qf.disabled = false;
  qg.disabled = false;
  qh.disabled = false;
  qa.style.color = "white";
  qb.style.color = "white";
  qc.style.color = "white";
  qd.style.color = "white";
  qe.style.color = "white";
  qf.style.color = "white";
  qg.style.color = "white";
  qh.style.color = "white";
  newQ();
}

function optA() {
if(que.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qa.disabled = true;
  qa.style.color = "black";
}
}
function optB() {
if(que2.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qb.disabled = true;
  qb.style.color = "black";
}
}
function optC() {
if(que3.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qc.disabled = true;
  qc.style.color = "black";
}
}
function optD() {
if(que4.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qd.disabled = true;
  qd.style.color = "black";
}
}
function optE() {
if(que5.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qe.disabled = true;
  qe.style.color = "black";
}
}
function optF() {
if(que6.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qf.disabled = true;
  qf.style.color = "black";
}
}
function optG() {
if(que7.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qg.disabled = true;
  qg.style.color = "black";
}
}
function optH() {
if(que8.rw){
  points++;
} else {
}
if(dblQ == 1){
q++;
if(dbchk){
  defButtons();
} else{
  if(!end){
newQ();
  } else {
    backToMenu();
  }
}
} else{
  dblQ--;
  qh.disabled = true;
  qh.style.color = "black";
}
}
