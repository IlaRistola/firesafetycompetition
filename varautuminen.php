<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
<head>
    <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Varautuminen</title>
    <style>
      html,
body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
}
.toplogo {
width: 25%;
height: auto;
display: block;
margin-left: auto;
margin-right: auto;
}

.visa-end {
  width: 50%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  background-color: rgba(187, 187, 187, 0.5);
  padding: 20px 25px;
}

.visaEndText{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}

.itemA { grid-area: a; }
.itemB { grid-area: b; }
.itemC { grid-area: c; }
.itemD { grid-area: d; }
.itemE { grid-area: e; }
.itemF { grid-area: f; }
.itemG { grid-area: g; }
.itemH { grid-area: h; }
.itemI { grid-area: i; }
.itemJ { grid-area: j; }
.itemK { grid-area: k; }
.itemL { grid-area: l; }
.itemM { grid-area: m; }
.itemN { grid-area: n; }
.itemO { grid-area: o; }
.itemP { grid-area: p; }
.itemQ { grid-area: q; }
.itemR { grid-area: r; }
.itemS { grid-area: s; }
.itemT { grid-area: t; }
.itemU { grid-area: u; }
.itemV { grid-area: v; }
.item5 { 
    grid-area: num;
    }
.item6 { grid-area: quest;
 }
.item7 { grid-area: qtext;
 }

.grid-outside {
  width: 50%;
  display: none;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  background-color: rgba(187, 187, 187, 0.5);
  padding: 20px 25px;
}

.grid-container {
  display: grid;
  grid-template-areas:
    'a b'
    'c d'
    'e f'
    'g h'
    'i j'
    'k l'
    'm n'
    'o p'
    'q r'
    's t'
    'u v';
  grid-gap: 5px;
  padding: 1px;
}
.question {
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: 10vw;
  grid-template-areas:
    'num quest quest quest quest'
    'num qtext qtext qtext qtext';
  grid-gap: 0em;
  grid-column-gap: 0em;
  grid-auto-flow: dense;
  height: auto;
  margin-bottom: 10px;
}
.question .item5, .question .item7{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    background-color: #1d1d1b;
    color: white;
    font-size: 1.2vw;
    height: 100%;
}
.question .item6{
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-style: italic;
    background-color: #1d1d1b;
    color: #d4d3d3;
    font-size: 1vw;
}

.question > div {
  text-align: left;
  padding: 3px 20px; 
  font-size: 18px;
  height: auto;
}

.qnum {
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
right: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

.grid-container .itemA2, .grid-container .itemB2, .grid-container .itemC2, .grid-container .itemD2{
  background-color: #1d1d1b;
  margin-left: auto;
  margin-right: auto;
  margin-top: auto;
  margin-bottom: auto;
}

.pics {
  width: 256px;
  height: 256px;
  background-color: white;
  margin-left: auto;
  margin-right: auto;
  margin-top: auto;
  margin-bottom: auto;
}

.logobottom {
position: fixed;
left: 50%;
bottom: 0px;
transform: translate(-50%, -50%);
width: 10%;
height: auto;
margin: 0 auto;
}
#guide{
    position: fixed;
    bottom: 0px;
    right: 0px;
    z-index: 1;
}
.btn-group{
    z-index: 2;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: block;
}
.btn-group button {
margin: 1.5vw;
padding: 10vh;
cursor: pointer;
float: left;
border-style: hidden;
}


#TBD {
opacity:1;
position: fixed;
z-index: 1;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
}
#quizPoints {
opacity:1;
display: none;
position: fixed;
z-index: 3;
top: 5%;
left: 15%;
font-size: 200%;
font-weight: bold;
border-style: solid;
background-color: lightgreen;
border-color: green;
color: navy;
transform: translate(-50%, -50%);
max-width: 100%;
max-height: 100%;
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

.startTestDiv {
  width: 50%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #1d1d1b;
  padding:5px 5px;
  position: relative;
  z-index: 2;
}

.startTestDiv .startBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: center;
    padding: 10px 5;
    margin-left: 40%;
    margin-bottom: 5%;
}
.misNote{
  background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 1.2vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5%;
}

#gameTime {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

@media screen and (max-width: 579px){
.btn-group{
    left: 54%;
}
#start {
background-size: 100%;
}
#startSwe {
background-size: 100%;
}
#startEng {
background-size: 100%;
}
}
    </style>

</head>

<body>

<img src="content/visuals/logoUp.png" id="ltop" class="toplogo">
<p id="gameTime"></p>
<div class="startTestDiv" id="startDivi">
    <div class="missionGuide"><div class="misNote" id="thisNote">Matkustatte viikonlopuksi mökille, jonka aikana myrsky yllättää. Myrskyn seurauksena mökin lähistöllä kulkevalle sähkölinjalle ja autotielle kaatuu puita. Mökille ei pääse autolla ja mökistä katkeaa sähköt. Joudutte viettämään mökillä kolme vuorokautta ilman sähköä ja ulkopuolista apua. 
      On talvi ja noin -10 astetta pakkasta. Mökki sijaitsee pienen lammen rannalla. Mökissä on takka ja ulkovessa.</div></div>
     <button class="startBut" id="stbut" onclick="startTest()">Aloita testi</button>
  </div>
<div class="grid-outside" id="gOutside">
  <div class="qnum" id="qRound">0/7</div>
    <div class="question"><div class="item5"></div><div class="item6">Valitse oikeat kuvat</div><div class="item7" id="description">Tehtävänänne on poimia valokuvien joukosta 7 tärkeintä tarviketta, joiden avulla pärjäisitte kolme vuorokautta edellä kuvatussa tilanteessa. Tarvikkeet valitaan painamalla kyseistä valokuvaa. Harkitse huolella, koska lukittuja valintoja ei voi muuttaa.</div></div>
  <div class="grid-container">
    <!-- Grid for puzzle -->
    <button class="itemA" id="ia2" onclick="optA()"><img class="pics" src="content/visuals/varautuminen/kuva1.png"></button>
    <button class="itemB" id="ib2" onclick="optB()"><img class="pics" src="content/visuals/varautuminen/kuva2.png"></button> 
    <button class="itemC" id="ic2" onclick="optC()"><img class="pics" src="content/visuals/varautuminen/kuva3.png"></button> 
    <button class="itemD" id="id2" onclick="optD()"><img class="pics" src="content/visuals/varautuminen/kuva4.png"></button>
    <button class="itemE" id="ie2" onclick="optE()"><img class="pics" src="content/visuals/varautuminen/kuva5.png"></button>
    <button class="itemF" id="if2" onclick="optF()"><img class="pics" src="content/visuals/varautuminen/kuva6.png"></button> 
    <button class="itemG" id="ig2" onclick="optG()"><img class="pics" src="content/visuals/varautuminen/kuva7.png"></button> 
    <button class="itemH" id="ih2" onclick="optH()"><img class="pics" src="content/visuals/varautuminen/kuva8.png"></button>
    <button class="itemI" id="ii2" onclick="optI()"><img class="pics" src="content/visuals/varautuminen/kuva9.png"></button>
    <button class="itemJ" id="ij2" onclick="optJ()"><img class="pics" src="content/visuals/varautuminen/kuva10.png"></button> 
    <button class="itemK" id="ik2" onclick="optK()"><img class="pics" src="content/visuals/varautuminen/kuva11.png"></button> 
    <button class="itemL" id="il2" onclick="optL()"><img class="pics" src="content/visuals/varautuminen/kuva12.png"></button>
    <button class="itemM" id="im2" onclick="optM()"><img class="pics" src="content/visuals/varautuminen/kuva13.png"></button>
    <button class="itemN" id="in2" onclick="optN()"><img class="pics" src="content/visuals/varautuminen/kuva14.png"></button> 
    <button class="itemO" id="io2" onclick="optO()"><img class="pics" src="content/visuals/varautuminen/kuva15.png"></button> 
    <button class="itemP" id="ip2" onclick="optP()"><img class="pics" src="content/visuals/varautuminen/kuva16.png"></button>
    <button class="itemQ" id="iq2" onclick="optQ()"><img class="pics" src="content/visuals/varautuminen/kuva17.png"></button> 
    <button class="itemR" id="ir2" onclick="optR()"><img class="pics" src="content/visuals/varautuminen/kuva18.png"></button>
    <button class="itemS" id="is2" onclick="optS()"><img class="pics" src="content/visuals/varautuminen/kuva19.png"></button>
    <button class="itemT" id="it2" onclick="optT()"><img class="pics" src="content/visuals/varautuminen/kuva20.png"></button> 
    <button class="itemU" id="iu2" onclick="optU()"><img class="pics" src="content/visuals/varautuminen/kuva21.png"></button> 
    <button class="itemV" id="iv2" onclick="optV()"><img class="pics" src="content/visuals/varautuminen/kuva22.png"></button> 
  </div>
</div>

<script src="scripts/varautuminen.js"></script>

</body>
</html>