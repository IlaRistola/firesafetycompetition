<?php
session_start();
 
if (isset($_GET['action']) && ('logout' == $_GET['action'])) {
    unset($_SESSION['id']);
    header('Location: index.php');
}
 
if (isset($_SESSION['id'])) {
    ?>
    <?php
} else { //redirect to login page
    header('Location: index.php');
}
?>
<html>
    <head>
        <title>Vaaranpaikat</title>
        <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <style>
    
#quizPoints {
opacity:1;
display: none;
position: fixed;
z-index: 2;
top: 5%;
right: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-touch-callout: none; /* iOS Safari */
-webkit-user-select: none; /* Safari */
-khtml-user-select: none; /* Konqueror HTML */
-moz-user-select: none; /* Old versions of Firefox */
-ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently
                          supported by Chrome, Edge, Opera and Firefox */
}

.startTestDiv {
  width: 50%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  /*background-color: rgba(187, 187, 187, 0.5);*/
  background-color: #1d1d1b;
  padding:5px 5px;
  position: relative;
  z-index: 2;
}

.startTestDiv .startBut{
    background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    font-size: 1.2vw;
    text-align: center;
    padding: 10px 5;
    margin-left: 40%;
    margin-bottom: 5%;
  /*transform: translate(-50%, -50%);*/
}
.misNote{
  background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 1.2vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5%;
}

#gameTime {
display: none;
opacity:1;
position: fixed;
z-index: 2;
top: 5%;
left: 5%;
font-size: 3vw;
font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
font-weight: bold;
font-style: italic;
color: white;
background-color: #1d1d1b;
padding: 5px 5px 5px;
-webkit-user-select: none;  /* Chrome all / Safari all */
-moz-user-select: none;     /* Firefox all */
-ms-user-select: none;      /* IE 10+ */
user-select: none;
}

    </style>

</head>

<body>
    <p id="quizPoints">0 / 7</p>
    <p id="gameTime"></p>
<div class="startTestDiv" id="startDivi">
    <div class="missionGuide"><div class="misNote" id="thisNote">Tutkikaa 360-kuvaa huoneesta ja etsikää 7 turvallisuusriskiä. Klikkaamalla hiirellä oikeita kohtia huoneessa löydätte riskit. Olkaa nopeita, aikaa on vain muutamia minuutteja!</div></div>
     <button class="startBut" id="stbut" onclick="dangerPlaces()">Aloita testi</button>
  </div>
  <!-- level happens in interactive 360 view -->
    <a-scene vr-mode-ui="enabled: false" loading-screen="enabled: false">

        <a-asset>
            <img id="stillView" preload="auto" src="content/visuals/vaaranpaikat2/room.jpg" crossOrigin="anonymous">
            <img id="p1" preload="auto" src="content/visuals/vaaranpaikat2/p1.png" crossOrigin="anonymous">
            <img id="p2" preload="auto" src="content/visuals/vaaranpaikat2/p2.png" crossOrigin="anonymous">
            <img id="p3" preload="auto" src="content/visuals/vaaranpaikat2/p3.png" crossOrigin="anonymous">
            <img id="p4" preload="auto" src="content/visuals/vaaranpaikat2/p4.png" crossOrigin="anonymous">
            <img id="p5" preload="auto" src="content/visuals/vaaranpaikat2/p5.png" crossOrigin="anonymous">
            <img id="p6" preload="auto" src="content/visuals/vaaranpaikat2/p6.png" crossOrigin="anonymous">
            <img id="p7" preload="auto" src="content/visuals/vaaranpaikat2/p7.png" crossOrigin="anonymous">
            <img id="end" preload="auto" src="content/visuals/vaaranpaikat2/end.png" crossOrigin="anonymous">
        </a-asset>

        <a-entity id="hsGroup1">
            <a-circle id="hotspot1" class="not-touchable" color="red" visible="false" position="6.5 0.5 2.7" rotation="0 -100 0" radius="0.5" opacity="0.8" alpha-test="0.7"></a-circle> <!--liesi-->
            <a-circle id="hotspot2" class="not-touchable" visible="false" color="blue" position="-3 -2.5 3" rotation="-20 -220 0" radius="0.7" opacity="0.8" alpha-test="0.7"></a-circle> <!--jatkojohto-->
            <a-circle id="hotspot3" class="not-touchable" visible="false" color="green" position="5 3.9 -3.2" rotation="20 -30 0" radius="0.7" opacity="0.8" alpha-test="0.7"></a-circle> <!--palohälytin-->
            <a-circle id="hotspot4" class="not-touchable" visible="false" color="yellow" position="-6 -2.5 -6.5" rotation="-10 50 0" radius="0.8" opacity="0.8" alpha-test="0.7"></a-circle> <!--tulitikut-->
            <a-circle id="hotspot5" class="not-touchable" color="red" visible="false" position="6.5 -1.5 2.7" rotation="0 -90 0" radius="0.5" opacity="0.8" alpha-test="0.7"></a-circle> <!--hella-->
            <a-circle id="hotspot6" class="not-touchable" visible="false" color="green" position="-1.8 -1.3 4.5" rotation="0 -200 0" radius="0.5" opacity="0.8" alpha-test="0.7"></a-circle> <!--kaasupullo-->
            <a-circle id="hotspot7" class="not-touchable" visible="false" color="blue" position="2.5 -1 7" rotation="0 -170 0" radius="0.7" opacity="0.8" alpha-test="0.7"></a-circle> <!--sähkölaturi-->
            </a-entity>
            <a-sky id="imgSphere" src="#stillView" rotation="0 0 0" visible="true" opacity="1">
            </a-sky>

            
            <a-entity look id="cameraWrapper" rotation="0 150 0" resetorientation>
                <a-camera look-controls mouse-cursor wasd-controls-enabled="false">
                    <a-entity cursor="rayOrigin: mouse;" raycaster="objects: .touchable"></a-entity>
                    <a-plane id="quiz" class="not-touchable" src="#p6" position="0 0 -1.5" scale="1 1 1" rotation="0 0 0" visible="false"
                    animation="property: scale; startEvents: ani; from: 0.1 0.1 0.1; to: 1 1 1; dur: 1000; easing: linear;" animation__2="property: rotation; startEvents: ani; from: 0 0 -540; to: 0 0 0; dur: 1000; easing: linear"></a-plane>
                </a-camera>
                </a-entity>
                </a-entity>

    </a-scene>

    <script src="scripts/vaaranpaikat.js"></script>
   
</body>

</html>