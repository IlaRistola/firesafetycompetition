<?php
session_start();
  
require_once('scripts/config.php');
  
//if user is logged in redirect to myaccount page
if (isset($_SESSION['id'])) {
    header('Location: myaccount.php');
}
  
$error_message = '';
if (isset($_POST['submit'])) {
 
    extract($_POST);
 
    if (!empty($password)) {
        $sql = "SELECT id, status FROM users WHERE password = '".md5($conn->real_escape_string($password))."'";
        $result = $conn->query($sql);
  
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if($row['status']) {
                $_SESSION['id'] = $row['id'];
                header('Location: myaccount.php');
            } else {
                $error_message = 'Your account is not active yet.';
            }
        } else {
            $error_message = 'Incorrect password.';
        }
    } else {
        $error_message = 'Please enter password.';
    }
}
?>
<html>
    <head>
        <title>Start screen</title>
        <script src="scripts/aframe.min.js"></script>
    <script src="scripts/chromakey.js"></script>
    <script src="scripts/roomOrient.js"></script>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <style>
        html,
        body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background-image: url('content/visuals/bgimage.png');
    background-repeat: no-repeat;
    background-position: center;
    }
    .container{
    width: 50%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: rgba(187, 187, 187, 0.5);
    position: fixed;
    z-index: 2;
    }
    .headNote{
        background-color: #1d1d1b;
    color: white;
    font-family: "Myriad Pro", Myriad Pro, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
  font-style: italic;
  text-align: center;
  font-size: 2.5vw;
  padding: 3px 0;
  width: 90%;
  margin-top: 3%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 3%;
    }
    .form-control{
  margin-top: 1.5%;
  margin-left: 25%;
  margin-right: auto;
  margin-bottom: 1.5%;
  font-size: 2vw;
  padding: 5px 5;

    }
    .btn{
  margin-top: 1.5%;
  margin-left: 40%;
  margin-right: auto;
  margin-bottom: 3%;
  font-size: 2.5vw;
  padding: 5px 5;
  background-color: #e0101d;
    color: white;
    font-family: "Myriad", Myriad, "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
    text-align: center;
    }
        </style>
    </head>
    <body>
        <div class="container">
                    <h3 class="headNote">SYÖTÄ OPETTAJAN SALASANA</h3>
                    <?php if(!empty($error_message)) { ?>
                        <div class="alert alert-danger"><?php echo $error_message; ?></div>
                    <?php } ?>
                    <form class="login-areas" method="post">
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Salasana" required />
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">PAINA</button>
                    </form>
                </div>
    </body>
</html>